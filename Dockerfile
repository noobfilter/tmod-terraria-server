FROM alpine:3.8 as builder

LABEL maintainer="snwflake <spam@snwflake.ml>"

WORKDIR /build/

# collect needed tools
RUN apk add wget unzip &&\
    mkdir -p /output/server &&\
    mkdir -p /output/ModLoader/Worlds &&\
    mkdir -p /output/ModLoader/Mods
# download vanilla server and prep for output
RUN wget terraria.org/server/terraria-server-1353.zip &&\
    unzip terraria-server-*.zip &&\
    cp -va 1353/Linux/. /output/server
# download tModLoader and prep for output
RUN wget https://github.com/blushiemagic/tModLoader/releases/download/v0.10.1.5/tModLoader.Linux.v0.10.1.5.zip &&\
    unzip tModLoader.Linux.v*.zip -d /output/server &&\
    chmod u+x /output/server/tModLoaderServer*

COPY world /output/ModLoader/Worlds/
COPY mods /output/ModLoader/Mods/
COPY server.conf /output/server/


FROM debian:9-slim

EXPOSE 7777
# VOLUME /terraria
CMD ["mono", "/terraria-server/tModLoaderServer.exe", "-config=server.conf"]

RUN apt-get update && apt-get install mono-complete -y

COPY --from=builder /output/server /terraria-server
COPY --from=builder /output/ModLoader /root/.local/share/Terraria/ModLoader